json.extract! product, :id, :name, :price, :quantity, :references, :references, :created_at, :updated_at
json.url product_url(product, format: :json)
