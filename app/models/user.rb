class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :validatable

         belongs_to :company

         ROLES = [['Admin', 'admin'], ['Moderator', 'moderator'], ['Marketing', 'marketing']]
end
