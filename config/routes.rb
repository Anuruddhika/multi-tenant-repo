Rails.application.routes.draw do
  # devise_for :users, controllers: { registrations: "users/registrations" }
  devise_for :users
  # devise_for :admins
  devise_for :admins, controllers: { registrations: 'admins/registrations' }

  root 'pages#index'

  resources :products
  resources :users
  # devise_for :users
  # devise_for :admins
  resources :companies
  resources :admin_dashboards
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
