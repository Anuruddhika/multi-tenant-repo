class Ability
    include CanCan::Ability

    def initialize(user)
        alias_action :create, :read, :update, :destroy, :to => :crud
        alias_action :read, :update, :destroy, :to => :rud
        user ||= User.new
        if user.role == "admin"
          can :manage, :all
        elsif user.role == "marketing"
          can :manage, Product
        elsif user.role == "moderator"
          cannot :manage, :all
        else
          cannot :manage, :all
        end
    end
end
