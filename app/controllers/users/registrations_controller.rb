class Users::RegistrationsController <Devise::RegistrationsController
	before_action :authenticate_admin!, :redirect_unless_admin

	private

	def redirect_unless_admin
		unless current_admin
			redirect_back(fallback_location: root_path)
		end
	end

	def sign_up_params
		params.require(:user).permit(:email, :password, :password_confirmation)
	end

	def account_update_params
		params.require(:user).permit(:email, :password, :password_confirmation, :current_password, :name, :phone, :avatar)
	end

	end
